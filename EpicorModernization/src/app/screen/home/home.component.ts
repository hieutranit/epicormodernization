import { Component, OnInit, ViewChild, AfterViewInit, TemplateRef } from '@angular/core';
import 'fullcalendar';
import { EventService } from '../../services/event.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {
  IEpButton,
  EpPosition,
  EpRenderMode
} from '@epicor/kinetic';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import {
  IEpViewModel,
  EpDialogService,
  EpDialogSize,
  EpDialogButtons,
  IEpGridModel
} from '@epicor/kinetic';
import { RelatedDocumentComponent } from '../related-document/related-document.component';
import { AddVehicleComponent } from '../add-vehicle/add-vehicle.component';
import { CommonService } from '../../services/common.service';
import { LeftMenuComponent } from '../../components';
import { SettingComponent } from '../../components';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  displayEvent: any;
  showDragThumb = false;
  public gridModel: IEpGridModel;
  public dataCombobox: Array<string> = ['Order', 'John Doe', 'John Doe', 'John Doe'];
  public dataBranches: Array<string> = ['(All Branches)', 'John Doe', 'John Doe', 'John Doe'];
  public dataStockLocations: Array<string> = ['(All Stock Locations)', 'John Doe', 'John Doe', 'John Doe'];
  public dataVehicleType: Array<string> = ['(All Vehicle Type)', 'John Doe', 'John Doe', 'John Doe'];

  @ViewChild('typeDocument') typeDocument: TemplateRef<any>;
  constructor(
    protected eventService: EventService,
    private commonservice: CommonService,
    private router: Router,
    private _httpService: Http,
    private spinner: NgxSpinnerService
  ) {
   }

  getDocument() {
    this.commonservice.getDocument().subscribe(
      res => { this.initFindDocument(res); }
    );
  }

  initFindDocument(dataDocument) {
    console.log(dataDocument);
    const model: IEpGridModel = {
      data: dataDocument,
      resizable: true,
      selectable: true,
      sortable: true,
      columns: [
        { field: 'type', title: '', width: 40, hidden: false },
        { field: 'number', title: 'Number', width: 200, hidden: false },
        { field: 'status', title: 'Status', width: 200, hidden: false },
        { field: 'branch', title: 'Branch', width: 200, hidden: false },
        { field: 'issuing', title: 'Issuing', width: 200, hidden: false },
        { field: 'via', title: 'Via', width: 200, hidden: false },
        { field: 'custommer_supplier_to_branch', title: 'Custommer/Supplier/To Branch', width: 200, hidden: false },
        { field: 'collection_required_date', title: 'Collection/Required Date', width: 200, hidden: false },
        { field: 'collection_requested_date', title: 'Collection/Requested Date', width: 200, hidden: false },
        { field: 'delivery_collection_area', title: 'Delivery/Collection Area', width: 200, hidden: false },
        { field: 'delivery_collection_city', title: 'Delivery/Collection City', width: 200, hidden: false },
        { field: 'total_weight', title: 'Total Weight (lb)', width: 200, hidden: false },
        { field: 'total_volume', title: 'Total Volume (MBF)', width: 200, hidden: false },
        { field: 'no_of_packs', title: 'No Of Packs', width: 200, hidden: false },
        { field: 'max_width', title: 'Max Width', width: 200, hidden: false },
        { field: 'max_length', title: 'Max Length', width: 200, hidden: false },
        { field: 'delivery_address', title: 'Delivery/ Address', width: 200, hidden: false },
        { field: 'special_instructions', title: 'Special Instructions', width: 200, hidden: false },
        { field: 'date_required_option', title: 'Date Required Option', width: 200, hidden: false },

      ]
    };
    this.gridModel = model;
    this.gridModel.columns[0].cellTemplate = this.typeDocument;
  }

  handlerContextFind(event) {
    if (event === 'Related document') {
      this.showDialogRelatedDocument();
    }
  }

  go() {
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 3000);
  }
  public handlerContextCalendar(event) {
    if (event === 'Add Vehicle') {
      this.showDialogAddVehicle();
    }
  }

  public showDialogRelatedDocument() {
    let buttons: EpDialogButtons | IEpButton[] = EpDialogButtons.Ok;
    buttons = [{ id: 'id1', label: 'Expand', primary: true }, { id: 'id2', label: 'Close' }];
    EpDialogService.custom({
      title: 'Related Documents',
      component: RelatedDocumentComponent,
      size: EpDialogSize.Small,
      buttons: buttons

    });
  }

  public showDialogAddVehicle() {
    let buttons: EpDialogButtons | IEpButton[] = EpDialogButtons.Ok;
    buttons = [{ id: 'id1', label: 'OK', primary: true }, { id: 'id2', label: 'Cencel' }];
    EpDialogService.custom({
      title: 'Journey Planner Vehicle & Driver',
      component: AddVehicleComponent,
      size: EpDialogSize.Medium,
      buttons: buttons
    });
  }

  // tslint:disable-next-line:member-ordering
  viewModel: IEpViewModel = {
    navbar: {
      title: 'Epicor Modernization',
      userContextMenu: {
        userName: 'User name',
        email: 'emailUser',
        options:
          [
            {
              description: 'Exit system',
              caption: 'logout',
              icon: 'mdi mdi-help-circle-outline'
            }
          ]
      }
    },
    sidebarLeft: {
      show: true,
      position: EpPosition.Left,
      mode: EpRenderMode.Push,
      component: LeftMenuComponent
    },
    sidebarRight: {
      show: false,
      position: EpPosition.Left,
      mode: EpRenderMode.Push,
      component: SettingComponent
    },
    footer: {
      visible: true
    },
  };

  ngOnInit() {
    this.getDocument();
  }

  ngAfterViewInit(): void {

  }

}
