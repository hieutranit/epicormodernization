import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UIControlsModule } from '../../components/ui-controls.module';
import { HomeComponent } from './home.component';
import { EpCoreModule } from '@epicor/kinetic';
import { GridModule } from '@progress/kendo-angular-grid';
import { CalendarComponent } from '../calendar/calendar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const routing: Routes = [
  { path: '', component: HomeComponent },
];
@NgModule({
  declarations: [
    HomeComponent,
    CalendarComponent
  ],
  imports: [
    BrowserAnimationsModule,
    RouterModule.forChild(routing),
    EpCoreModule,
    UIControlsModule,
    GridModule,
  ],
  exports: []
})
export class HomeModule { }
