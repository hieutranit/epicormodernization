import { Component } from '@angular/core';
import { IEpDialogComponent, EpDialogInstance } from '@epicor/kinetic';

@Component({
  selector: 'app-related-document',
  templateUrl: './related-document.component.html',
  styleUrls: ['./related-document.component.scss']
})
export class RelatedDocumentComponent implements IEpDialogComponent {
  instance: EpDialogInstance;
  public expandedKeys: any[] = ['0', '0_0'];
  public data: any[] = [
    {
      text: 'Call Off Order 7028455',
      items: [
        {
          text: 'Call Off Order 7028455',
          items: [
            {
              text: 'Call Off Order 7028455',
              items: [
                {
                  text: 'Call Off Order 7028455',
                }
              ]
            },
            {
              text: 'Call Off Order 7028455',
              items: [
                {
                  text: 'Call Off Order 7028455',
                }
              ]
            },
            {
              text: 'Call Off Order 7028455',
              items: [
                {
                  text: 'Call Off Order 7028455',
                }
              ]
            },
            {
              text: 'Call Off Order 7028455',
              items: [
                {
                  text: 'Call Off Order 7028455',
                }
              ]
            },
            {
              text: 'Call Off Order 7028455',
              items: [
                {
                  text: 'Call Off Order 7028455',
                }
              ]
            }, {
              text: 'Call Off Order 7028455',
              items: [
                {
                  text: 'Call Off Order 7028455',
                }
              ]
            }
          ]
        }
      ]
    }
  ];
  constructor() { }

}
