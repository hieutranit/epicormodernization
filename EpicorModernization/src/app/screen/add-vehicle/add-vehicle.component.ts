import { Component } from '@angular/core';
import {EpDialogInstance, EpDialogService, IEpDialogComponent} from '@epicor/kinetic';
@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.scss']
})
export class AddVehicleComponent implements IEpDialogComponent {
  instance: EpDialogInstance;
  constructor() { }

  public selectedValues = {
    complex: null,
    complexWithTemplate: null,
    simpleStringArray: '',
    withEvents: null
  };

  public devEXF: Array<{ empName: string, empId: string }> = [
    { empName: 'John Doe', empId: '1' },
    { empName: 'Nancy Truman', empId: '2' },
    { empName: 'Sean Chen', empId: '3' },
    { empName: 'Simon Chau', empId: '4' },
    { empName: 'Tara Cantrok', empId: '5' },
    { empName: 'Peter Ferrero', empId: '6' },
    { empName: 'Matthew Hawkins', empId: '7' },
  ];
}
