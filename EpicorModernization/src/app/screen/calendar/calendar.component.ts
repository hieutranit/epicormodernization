import { Component, OnInit, Output, EventEmitter, AfterViewInit, AfterViewChecked } from '@angular/core';
import * as $ from 'jquery';
import 'jqueryui';
import 'fullcalendar';
import 'fullcalendar-scheduler';
import 'moment';
import {CommonService} from '../../services/common.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, AfterViewInit {

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onDrag = new EventEmitter();
  obsTextMsg: Observable<string>;
  constructor(private commonservice: CommonService) {
  }

  ngOnInit() {
    this.getResources();
  }

  getResources() {
    this.obsTextMsg = this.commonservice.getResource();
    this.commonservice.getResource().subscribe(
      res => {this.initCalendar(res); }
    );
  }

  initCalendar(data) {
    console.log('resourceData', data.resourceData);
    console.log('eventData', data.eventData);
    const seft = this;
    $(function () {
      const containerEE: JQuery = <any>$('#external-events tbody tr');
      $(containerEE).each(function () {

        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
          title: $.trim($(this).text()), // use the element's text as the event title
          stick: true // maintain when user navigates (see docs on the renderEvent method)
        });

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex: 9999,
          revert: true,      // will cause the event to go back to its
          revertDuration: 0,  //  original position after the drag
          cursor: 'move',
          start: () => {
            seft.drag(true);
          },
          stop: () => {
            seft.drag(false);
          }
        });

      });
      const containerEl: JQuery = <any>$('#calendar');

      containerEl.fullCalendar({
        titleFormat: 'dddd d MMMM YYYY',
        now: '2018-02-07',
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        editable: true, // enable draggable events
        aspectRatio: 1.8,
        scrollTime: '06:00:00', // undo default 6am scrollTime
        droppable: true, // this allows things to be dropped onto the calendar
        header: {
          left: 'today prev,next',
          right: 'title',
          // right: 'agendaDay,timelineDay,timelineThreeDays,agendaWeek,month,listWeek'

        },
        defaultView: 'agendaDay',
        views: {
          agenda: {
          }
        },
        eventRender: function (event, element) {
        },
        eventClick: function (calEvent, jsEvent, view) {
          // console.log('calEvent', calEvent);
          // console.log('jsEvent', jsEvent);
          // console.log('view', view);
        },
        eventReceive: function(event) {
          // const title = event.title;
          // alert(title);

          // const start = event.start.format('YYYY-MM-DD[T]HH:MM:SS');
          // $.ajax({
          //   url: 'process.php',
          //   data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
          //   type: 'POST',
          //   dataType: 'json',
          //   success: function(response){
          //     event.id = response.eventid;
          //     $('#calendar').fullCalendar('updateEvent',event);
          //   },
          //   error: function(e){
          //     console.log(e.responseText);
          //   }
          // });
          containerEl.fullCalendar('updateEvent', event);
        },
        resourceLabelText: 'Rooms',
        resources: data.resourceData,
        events: data.eventData
      });

      const allDay: JQuery = <any>$('.fc-agenda-view .fc-day-grid');
      $(allDay).css('display', 'none');
    });
  }

  public customListBoxTime() {
    const paneContentScroll = document.querySelector('.fc-scroller.fc-time-grid-container');
    const paneHeaderScroll = document.querySelector('.fc-head');
    const paneBody = document.querySelector('.fc-body');
    const listBoxTime = this.generateListBoxTime();
    paneBody.insertBefore(listBoxTime, paneBody.childNodes[0]);
    paneContentScroll.addEventListener('scroll', (event) => {
      const scrollLeft = paneContentScroll.scrollLeft;
      const scrollTop = paneContentScroll.scrollTop;
      paneHeaderScroll.scrollLeft = scrollLeft;
      listBoxTime.scrollTop = scrollTop;
    });
  }

  public generateListBoxTime(): HTMLElement {
    const boxTime = this.generateBoxTime();
    const listBoxTimeOld = document.querySelectorAll('.fc-axis.fc-time.fc-widget-content');
    for (let index = 0; index < listBoxTimeOld.length; index++) {
      const itemItem = document.createElement('div');
      const textNode = document.createTextNode(listBoxTimeOld[index].textContent);
      itemItem.className = 'ca-item-time';
      itemItem.appendChild(textNode);
      boxTime.appendChild(itemItem);
    }
    return boxTime;
  }

  public generateBoxTime(): HTMLElement {
    const boxTime = document.createElement('div');
    boxTime.className = 'ca-list-item';
    return boxTime;
  }

  public drag(value: boolean) {
    this.onDrag.emit(value);
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.customListBoxTime();
    }, 1000);
  }

}
