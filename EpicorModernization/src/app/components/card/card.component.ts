import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() title: string;
  @Input() btn: boolean;

  constructor() { }

  ngOnInit() {
  }

}
