import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import {
  CardModule,
  ContextMenuModule,
  DragThumbModule,
  ResizeModule,
  LeftMenuModule,
  SettingModule
} from '.';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { SettingComponent } from './setting/setting.component';


const UI_MODULES = [
  CardModule,
  ContextMenuModule,
  DragThumbModule,
  ResizeModule,
  LeftMenuModule,
  SettingModule
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    HttpModule,
    ...UI_MODULES

  ],
  exports: [
    ...UI_MODULES

  ],
  entryComponents: [],
  declarations: []
})
export class UIControlsModule { }
