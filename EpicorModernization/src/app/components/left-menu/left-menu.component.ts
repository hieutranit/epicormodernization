import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';



@Component({
  selector: 'left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {
  @Input() data: any;
  constructor() { }

  ngOnInit() {
  }

}
