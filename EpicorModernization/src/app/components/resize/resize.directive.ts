import {
  Directive,
  ElementRef,
  Renderer2,
  HostListener,
  AfterViewInit
} from '@angular/core';

@Directive({
  selector: '[resize]'
})
export class ResizeDirective implements AfterViewInit {

  isMove = false;
  heightOld: any;
  mouseStart: MouseEvent;
  mouseEnd: MouseEvent;
  constructor(public elementRef: ElementRef, public render: Renderer2) {

  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.style.paddingBottom = '5px';
    const thumb = document.createElement('div');
    thumb.classList.add('resize-thumb');
    this.elementRef.nativeElement.appendChild(thumb);
    this.elementRef.nativeElement.classList.add('p-relative');

    this.render.listen(thumb, 'mousedown', (event) => {
      this.isMove = true;
      this.mouseStart = event;
      this.heightOld = this.elementRef.nativeElement.clientHeight;
      event.preventDefault();
    });

    this.render.listen('body', 'mouseup', (event) => {
      this.isMove = false;
    });
    this.render.listen('body', 'mousemove', (event) => {
      this.onMouseDrag(event);
    });
  }

  public onMouseDrag(event: MouseEvent) {
    if (this.isMove) {
      this.mouseEnd = event;
      const height = this.mouseEnd.clientY - this.mouseStart.clientY + this.heightOld;
      this.elementRef.nativeElement.style.height = height + 'px';
    }
  }
}
