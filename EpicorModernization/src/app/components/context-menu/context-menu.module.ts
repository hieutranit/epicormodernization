import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContextMenuComponent } from './context-menu.component';
import { PopupModule } from '@progress/kendo-angular-popup';

@NgModule({
    declarations: [ContextMenuComponent],
    imports: [
        CommonModule, PopupModule
    ],
    exports: [ContextMenuComponent]
})
export class ContextMenuModule {

}
