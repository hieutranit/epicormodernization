import {
  Component,
  OnInit,
  ElementRef,
  HostListener,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss']
})
export class ContextMenuComponent implements OnInit {
  // Input
  @Input() menu: any;

  // Output
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onItemClick = new EventEmitter<KeyboardEvent>();

  offset: any;
  show: boolean;
  constructor(private elementRef: ElementRef) {
    this.offset = { left: 30, top: 30 };
  }

  ngOnInit() {
  }

  @HostListener('contextmenu', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    this.showContextMenu(event);
  }
  /**
   * Show Context Menu.
   * @param mouse : Mouse Event.
   */
  private showContextMenu(event: MouseEvent) {
    this.show = true;
    this.offset = { top: event.pageY, left: event.pageX };
    event.preventDefault();
  }

  @HostListener('document:click', ['$event', '$event.target'])
  onClickOfDocumnet(event: MouseEvent, targetElement: HTMLElement): void {
    this.hiddenContextMenu();
  }

  /**
    * Hidden Context Menu.
    */
  private hiddenContextMenu() {
    this.show = false;
  }

  private onClickHandler(item: any) {
    this.onItemClick.emit(item);
  }
}
