import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { UIControlsModule } from './ui-controls.module';

platformBrowserDynamic().bootstrapModule(UIControlsModule);
