import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingComponent } from './setting.component';

@NgModule({
    declarations: [SettingComponent],
    imports: [
        CommonModule
    ],
    exports: [SettingComponent],
    entryComponents: [SettingComponent]
})
export class SettingModule {

}
