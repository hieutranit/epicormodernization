import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragThumbComponent } from './drag-thumb.component';
import { PopupModule } from '@progress/kendo-angular-popup';

@NgModule({
  declarations: [DragThumbComponent],
  imports: [
    CommonModule, PopupModule
  ],
  exports: [DragThumbComponent]
})
export class DragThumbModule {

}
