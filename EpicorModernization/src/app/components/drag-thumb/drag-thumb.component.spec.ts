import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragThumbComponent } from './drag-thumb.component';

describe('DragThumbComponent', () => {
  let component: DragThumbComponent;
  let fixture: ComponentFixture<DragThumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragThumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragThumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
