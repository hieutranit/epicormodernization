import {
  Component,
  OnInit,
  Input,
  ElementRef,
  Renderer2,
  HostListener
} from '@angular/core';

@Component({
  selector: 'drag-thumb',
  templateUrl: './drag-thumb.component.html',
  styleUrls: ['./drag-thumb.component.scss']
})
export class DragThumbComponent implements OnInit {

  constructor(public elementRef: ElementRef, render2: Renderer2) { }

  @Input() isShow: Boolean = false;

  ngOnInit() {
  }

  @HostListener('body:mousemove', ['$event'])
  onMousemove(event: MouseEvent) {
    if (this.isShow) {
      this.elementRef.nativeElement.style.display = 'block';
      this.elementRef.nativeElement.style.transform = `translate(${event.clientX}px, ${event.clientY}px)`;
    } else {
      this.elementRef.nativeElement.style.display = 'none';
    }
  }
}
