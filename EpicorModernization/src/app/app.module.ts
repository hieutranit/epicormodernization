import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { CoinService } from './services/coin.service';
import { EventService } from './services/event.service';
import { CalendarComponent } from './screen/calendar/calendar.component';
import { PopupModule } from '@progress/kendo-angular-popup';
import { EpCoreModule } from '@epicor/kinetic';
import { GridModule } from '@progress/kendo-angular-grid';
import { HomeComponent } from './screen/home/home.component';
import { RelatedDocumentComponent } from './screen/related-document/related-document.component';
import { CreateComponent } from './screen/create/create.component';
import { EditComponent } from './screen/edit/edit.component';
import { IndexComponent } from './screen/index/index.component';
import { UIControlsModule } from './components/ui-controls.module';
import {AddVehicleComponent} from './screen/add-vehicle/add-vehicle.component';
import { CommonService } from './services/common.service';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreateComponent,
    EditComponent,
    IndexComponent,
    CalendarComponent,
    RelatedDocumentComponent,
    AddVehicleComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    EpCoreModule,
    PopupModule,
    GridModule,
    UIControlsModule,
    NgxSpinnerModule
  ],
  providers: [CoinService, EventService, CommonService],
  bootstrap: [AppComponent],
  entryComponents: [
    RelatedDocumentComponent,
    AddVehicleComponent
  ]
})
export class AppModule { }
