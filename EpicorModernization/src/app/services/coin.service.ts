import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class CoinService {

    result: any;
    constructor(private http: HttpClient) { }

    addCoin(name, price) {
        const uri = '/api/todo';
        const obj = {
            name: name,
            price: price
        };
        this
            .http
            .post(uri, obj)
            .subscribe(res =>
                console.log('Done'));
    }

    getCoins() {
        const uri = '/api/todo';
        return this
            .http
            .get(uri)
            .map(res => {
                console.log(res);
                return res;
            });
    }

    editCoin(id) {
        const uri = '/api/todo/' + id;
        return this
            .http
            .get(uri)
            .map(res => {
                return res;
            });
    }

    updateCoin(name, price, id) {
        const uri = '/api/todo/' + id;

        const obj = {
            id: id,
            name: name,
            price: price
        };
        console.log("obj", obj);
        this
            .http
            .put(uri, obj)
            .subscribe(res => console.log('Done'));
    }

    deleteCoin(id) {
        const uri = '/api/todo/' + id;

        return this
            .http
            .delete(uri)
            .map(res => {
                return res;
            });
    }
}
