import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class CommonService {

  private dataUrl: string;
  constructor(private http: HttpClient) {
    this.dataUrl = `${environment.HOST_URL}`;
  }

  getResource(): Observable<any> {
    const uri = `${this.dataUrl}/calendar`;
    return this.http.get(uri);
  }

  getDocument(): Observable<any> {
    const uri = `${this.dataUrl}/products`;
    return this.http.get(uri);
  }
}
